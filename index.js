function getSum (num1, num2){
	console.log("Displayed sum of "+ num1 + " and " + num2);
	console.log(num1+num2);
}

getSum(5,15);

function getDifference (num1, num2){
	console.log("Displayed difference of "+ num1 + " and " + num2);
	console.log(num1-num2);
}

getDifference(20,5);

function getProduct (num1, num2){
	let product = num1 * num2;
	console.log ("The product of " + num1 + " and " + num2 + ":");
	return product;
}

let ansProduct = getProduct(50,10);
console.log(ansProduct);

function getQuotient (num1, num2){
	let quotient = num1/num2;
	console.log ("The quotient of " + num1 + " and " + num2 + ":");
	return quotient;
}

let ansQuotient = getQuotient(50,10);
console.log(ansQuotient);

function getCircleArea (radius){
	let circleArea = (3.141 * (radius**2));
	console.log ("The result of getting the area of a circle with 15 radius:");
	return circleArea;
}
let areaOfCircle = getCircleArea(15);
console.log(areaOfCircle);

function getAverage(num1, num2, num3, num4){
	console.log("The average of "+ num1 + "," + num2 + "," + num3 + "," + num4+ ":");
	let averageVar = ((num1 + num2 + num3 +num4)/4);
	return averageVar;
}

let averageVar = getAverage(20,40,60,80);
console.log(averageVar)


function identify(num1, num2){
    console.log("Is " + num1 + "/"+num2+ " a passing score?");
    percent = (num1/num2)*100;
    isPassed = (percent > 75);
    return isPassed;
}

let isPassingScore = identify(38, 50);
console.log(isPassingScore)
